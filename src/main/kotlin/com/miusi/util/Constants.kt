package com.miusi.util

object Constants {

    const val DATABASE_NAME = "stream_social_network"
    const val DEFAULT_POST_PAGE_SIZE = 15
}