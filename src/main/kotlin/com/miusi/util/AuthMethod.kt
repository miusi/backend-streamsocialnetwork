package com.miusi.util

sealed class AuthMethod() {
    object Email: AuthMethod()
    object Username: AuthMethod()
}
