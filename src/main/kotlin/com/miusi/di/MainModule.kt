package com.miusi.di

import com.miusi.data.repository.follow.FollowRepository
import com.miusi.data.repository.follow.FollowRepositoryImpl
import com.miusi.data.repository.user.UserRepository
import com.miusi.data.repository.user.UserRepositoryImpl
import com.miusi.service.UserService
import com.miusi.util.Constants
import org.koin.dsl.module
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo



val mainModule = module {
    single {
        val client = KMongo.createClient().coroutine
        client.getDatabase(Constants.DATABASE_NAME)
    }
    single<UserRepository>{
        UserRepositoryImpl(get())
    }
    single<FollowRepository>{
        FollowRepositoryImpl(get())
    }
    single { UserService(get()) }
}