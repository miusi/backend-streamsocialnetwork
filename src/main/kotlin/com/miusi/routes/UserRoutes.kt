package com.miusi.routes

import com.miusi.data.repository.user.UserRepository
import com.miusi.data.models.User
import com.miusi.data.requests.CreateAccountRequest
import com.miusi.data.requests.LoginRequest
import com.miusi.data.responses.BasicApiResponse
import com.miusi.service.UserService
import com.miusi.util.ApiResponseMessages
import com.miusi.util.ApiResponseMessages.FIELDS_BLANK
import com.miusi.util.ApiResponseMessages.INVALID_CREDENTIALS
import com.miusi.util.ApiResponseMessages.USER_ALREADY_EXISTS
import com.miusi.util.AuthMethod
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.createUserRoute(userService: UserService) {

    post("/api/user/create") {
        val request = call.receiveOrNull<CreateAccountRequest>() ?: kotlin.run {
            call.respond(HttpStatusCode.BadRequest)
            return@post
        }
        if (userService.doesUserWithEmailExist(request.email)) {
            call.respond(
                BasicApiResponse(successful = false, message = USER_ALREADY_EXISTS)
            )
            return@post
        }

        when (userService.validateCreateAccountRequest(request)) {
            is UserService.ValidationEvent.ErrorFieldEmpty -> {
                call.respond(
                    BasicApiResponse(
                        successful = false, message = FIELDS_BLANK
                    )
                )
                return@post
            }
            is UserService.ValidationEvent.Success -> {
                userService.createUser(request)
                call.respond(
                    BasicApiResponse(successful = true)
                )
            }
        }

    }
}

fun Route.loginUser(userRepository: UserRepository) {
    post("/api/user/login") {
        val request = call.receiveOrNull<LoginRequest>() ?: kotlin.run {
            call.respond(HttpStatusCode.BadRequest)
            return@post
        }

        if (request.email.isBlank() || request.password.isBlank()) {
            call.respond(HttpStatusCode.BadRequest)
            return@post
        }

        val isCorrectPassword = userRepository.doesPasswordForUserMatch(
            email = request.email,
            enteredPassword = request.password
        )

        if (isCorrectPassword) {
            call.respond(
                HttpStatusCode.OK,
                BasicApiResponse(
                    successful = true
                )
            )
        } else {
            call.respond(
                HttpStatusCode.OK,
                BasicApiResponse(
                    successful = true,
                    message = INVALID_CREDENTIALS
                )
            )
        }
    }
}