package com.miusi.plugins

import com.miusi.data.repository.follow.FollowRepository
import com.miusi.data.repository.post.PostRepository
import com.miusi.data.repository.user.UserRepository
import com.miusi.routes.*
import com.miusi.service.FollowService
import com.miusi.service.PostService
import com.miusi.service.UserService
import io.ktor.routing.*
import io.ktor.application.*
import org.koin.ktor.ext.inject

fun Application.configureRouting() {
    val userRepository: UserRepository by inject()
    val userService: UserService by inject()
    val followService: FollowService by inject()
    val postService: PostService by inject()

    routing {
        // User routes
        createUserRoute(userService)
        loginUser(userRepository)

        // Following routes
        followUser(followService)
        unfollowUser(followService)

        // Post routes
        createPostRoute(postService)
    }
}
