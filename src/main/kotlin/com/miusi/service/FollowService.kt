package com.miusi.service

import com.miusi.data.repository.follow.FollowRepository
import com.miusi.data.requests.FollowUpdateRequest

class FollowService(
    private val followRepository: FollowRepository
) {

    suspend fun followUserIfExists(request: FollowUpdateRequest) : Boolean {
        return followRepository.followUserIfExists(
            request.followingUserId,
            request.followedUserId
        )
    }

    suspend fun unfollowUserIfExists(request: FollowUpdateRequest) : Boolean {
        return followRepository.unFollowUserIfExists(
            request.followingUserId,
            request.followedUserId
        )
    }






}