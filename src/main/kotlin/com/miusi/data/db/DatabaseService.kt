package com.miusi.data.db

import com.miusi.data.models.User

interface DatabaseService {

     suspend fun createUser(user: User)

     suspend fun getUserById(id: String): User
}