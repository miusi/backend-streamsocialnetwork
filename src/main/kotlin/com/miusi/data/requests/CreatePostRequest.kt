package com.miusi.data.requests

data class CreatePostRequest(
    val userId: String,
    val description: String
)
