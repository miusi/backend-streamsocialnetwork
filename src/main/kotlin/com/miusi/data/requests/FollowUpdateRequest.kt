package com.miusi.data.requests


data class FollowUpdateRequest(
    val followedUserId: String,
    val followingUserId: String,
    )
