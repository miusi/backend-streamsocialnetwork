val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val koin_version: String by project
val kmongo_version: String by project

plugins {
    application
    kotlin("jvm") version "1.5.31"
}

group = "com.miusi"
version = "0.0.1"
application {
    mainClass.set("io.ktor.server.netty.EngineMain")
}

repositories {
    mavenCentral()
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().all {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation("io.ktor:ktor-server-core:1.6.4")
    implementation("io.ktor:ktor-websockets:1.6.4")
    implementation("io.ktor:ktor-gson:1.6.4")
    implementation("io.ktor:ktor-server-host-common:1.6.4")
    implementation("io.ktor:ktor-auth:1.6.4")
    implementation("io.ktor:ktor-auth-jwt:1.6.4")
    implementation("io.ktor:ktor-server-netty:1.6.4")
    implementation("ch.qos.logback:logback-classic:1.2.6")

    // KMongo
    implementation("org.litote.kmongo:kmongo:$kmongo_version")
    implementation("org.litote.kmongo:kmongo-coroutine:$kmongo_version")

    // Koin core features
    implementation("io.insert-koin:koin-core:$koin_version")

    // Koin for Ktor
    implementation("io.insert-koin:koin-ktor:$koin_version")
    implementation("io.insert-koin:koin-logger-slf4j:$koin_version")


    // Test dependencies

    // Gson
    testImplementation("com.google.code.gson:gson:2.8.8")

    // Koin
    testImplementation("io.insert-koin:koin-test:$koin_version")

    // Kotlin Test
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")

    // Ktor Test
    testImplementation("io.ktor:ktor-server-tests:1.6.4")

    // Truth
    testImplementation("com.google.truth:truth:1.1.3")





}